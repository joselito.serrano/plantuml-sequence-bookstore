# PlantUML-Sequence-Bookstore

You are designing a system for an online bookstore. The system should allow users to browse books, add books to their cart, and purchase books. The system should also allow administrators to add and remove books from the store.

Create a sequence diagram using PlantUML that illustrates the following scenario:

    A user logs in to the system and browses the store.
    The user adds a book to their cart.
    The user proceeds to checkout and completes the purchase.

 Make sure to include all the necessary actors, objects, and methods in your diagram. You can assume that the user is already authenticated and has a valid session token.

```plantuml
@startuml BookStore

actor User
boundary "User\nInterface" as UserInterface
database "Book\nDatabase" as BookDatabase

database "Cart\nDatabase" as CartDatabase

database "Payment\nDatabase" as PaymentDatabase

autonumber

User -> UserInterface ++: Browse Store
    UserInterface -> BookDatabase ++: GET /api/v1/books
    BookDatabase --> UserInterface --: HTTP 200
UserInterface --> User --: Show books


User -> UserInterface ++: Add book to cart
    UserInterface -> CartDatabase ++: POST /api/v1/cart/new/book:id
    CartDatabase --> UserInterface --: HTTP 201 
UserInterface --> User --: Book added to cart


User -> UserInterface ++: Checkout    
    UserInterface -> PaymentDatabase ++: POST /api/v1/payment/new/cart:id    
    PaymentDatabase --> UserInterface --: HTTP 201
    
    opt Payment Information Changed
        UserInterface -> PaymentDatabase ++: PUT /api/v1/payment:id
        PaymentDatabase --> UserInterface --: HTTP 201
    end
    
UserInterface --> User --: Purchase Completed

@enduml
```
